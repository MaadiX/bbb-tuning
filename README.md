# BBB Tuning

Custom configurations for BigBlueButton from privacy and customization docs, including:
* Some freeswitch tunning to avoid audio crackling
* Disable alone/muted/unmuted sounds
* Private recordings, only available for OWNERS and SHARED
** Code from https://github.com/ichdasich/bbb-rec-perm
* Cron to clean old recordings (older than 15 days)
* Custom retention days to clean presentation, caches, etc. Set to 2 days
* Custom retention days for logs. Set to 2 days
* Loglevel to ERROR or to /dev/null for BBB/nginx/freeswitch/red5/kurento logs

This configuration files have been tested over a standard installation of BBB release 2.2.29
To check your installed release

~~~
bbb-conf --help
~~~

### Prepare

Before copying this files you have to replace some PLACEHOLDERS with your data.

Replace FQDN string in all files with your fqdn with 
`grep -rl FQDN --exclude-dir=.git . | xargs sed -i s@FQDN@MYFQDN@g`

Get your BBBSECURITYSALT 
`grep securitySalt /usr/share/bbb-web/WEB-INF/classes/bigbluebutton.properties`

Replace BBBSECURITYSALT in `usr/share/bbb-web/WEB-INF/classes/bigbluebutton.properties`

Get your BBBAPPSAKKASHAREDSECRET 
`grep sharedSecret /usr/share/bbb-apps-akka/conf/application.conf`

Replace BBBAPPSAKKASHAREDSECRET in `usr/share/bbb-apps-akka/conf/application.conf`

Get FREESWITCHPASSWD 
`grep password /usr/share/bbb-fsesl-akka/conf/application.conf`

Replace FREESWITCHPASSWD in `usr/share/bbb-fsesl-akka/conf/application.conf`

Get POSGRESPASSWD 
`grep DB_PASSWORD /root/greenlight/.env`

Replace POSGRESPASSWD in `var/www/html/gl-auth/auth-passwd-bbb.py`

### Apply conf

Copy all files using rsync:

~~~
rsync -avz --exclude ".git" --exclude 'LICENSE' --exclude "README.md" . /
~~~

Restart BBB + nginx:

~~~
bbb-conf --restart
service nginx restart
~~~
